export const sortedData = (data) => data.sort((a, b) => {
    const dateA = new Date(a.attributes.deadLine);
    const dateB = new Date(b.attributes.deadLine);
    return dateA - dateB;
  });


export const formatDate = (dateString) => {
    const date = new Date(dateString);
    const day = String(date.getDate()).padStart(2, '0');
    const month = String(date.getMonth() + 1).padStart(2, '0');
    const year = date.getFullYear();
    return `${day}/${month}/${year}`;
  };

export const formattedDate = (date) => {
 return `${date.getFullYear()}-${(date.getMonth() + 1).toString().padStart(2, '0')}-${date.getDate().toString().padStart(2, '0')}`;
}
