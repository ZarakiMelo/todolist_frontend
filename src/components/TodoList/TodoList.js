import React from 'react';
import Task from '../Task/Task'
import './TodoList.css'
import { sortedData } from '../../utils/utils';

const TodoList = (props) => {

const {tasks, deleteTask}=props;

  const tasksToDisplay = sortedData(tasks).map((task) => {
    return <Task key={task.id} id={task.id} name={task.attributes.name} description={task.attributes.description} deleteTask={deleteTask} deadLine={task.attributes.deadLine}/>
  })

  return (
    <div className='list_container'>
       {tasksToDisplay}
    </div>
  );
};

export default TodoList;
