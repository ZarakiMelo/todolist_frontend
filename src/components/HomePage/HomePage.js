import React, { useEffect, useState, useCallback } from 'react';
import Header from '../Header/Header';
import Footer from '../Footer/Footer';
import TodoList from '../TodoList/TodoList';
import AddTask from '../AddTask/AddTask';
import './HomePage.css';
import { fetchTasks, addTask, deleteTask } from '../../api';
import { sortedData } from '../../utils/utils';

/**
 * HomePage Component
 * 
 * This component serves as the main page of the application, responsible for displaying tasks,
 * adding new tasks, and deleting tasks. It utilizes several child components such as Header, Footer,
 * TodoList, and AddTask.
 */
const HomePage = () => {
  // State to store the list of tasks
  const [tasks, setTasks] = useState([]);

  // State to manage the visibility of the AddTask component
  const [openAddTaskContent, setOpenAddTaskContent] = useState(false);

  // Auth token for API requests
  const authToken = process.env.REACT_APP_AUTH_TOKEN;

  /**
   * Fetches all tasks from the API and sets the tasks state
   * Uses useCallback to memoize the function and avoid unnecessary re-renders
   */
  const fetchAllTasks = useCallback(() => {
    fetchTasks()
      .then(response => {
        setTasks(sortedData(response.data.data)); // Sorts the tasks by deadline
      })
      .catch(error => {
        console.error('There was an error fetching the tasks!', error);
      });
  }, []);

  // Effect hook to fetch tasks when the component mounts
  useEffect(() => {
    fetchAllTasks();
  }, [fetchAllTasks]);

  /**
   * Handles the addition of a new task
   * @param {Object} newTask - The new task to be added
   */
  const handleAddTask = (newTask) => {
    addTask(newTask)
      .then(response => {
        console.log('Task added successfully:', response.data);
        fetchAllTasks(); // Refreshes the task list
        setOpenAddTaskContent(false); // Closes the AddTask component
      })
      .catch(error => {
        console.error('There was an error adding the task!', error);
      });
  };

  /**
   * Handles the deletion of a task
   * @param {number} id - The id of the task to be deleted
   */
  const handleDeleteTask = (id) => {
    deleteTask(id)
      .then(response => {
        console.log('Task deleted successfully:', response.data);
        // Removes the deleted task from the tasks state
        const newTasks = tasks.filter(task => task.id !== id);
        setTasks(newTasks);
      })
      .catch(error => {
        console.error('There was an error deleting the task!', error);
      });
  };

  /**
   * Toggles the visibility of the AddTask component
   */
  const handleToggleAddTaskContent = () => {
    setOpenAddTaskContent(!openAddTaskContent);
  };

  return (
    <div className='container'>
      <Header />
      <div className='main'>
        {/* Button to open the AddTask component */}
        {!openAddTaskContent && (
          <div className='addTaskButton_container'>
            <button type='button' onClick={handleToggleAddTaskContent}>
              Nouvelle tâche
            </button>
          </div>
        )}
        {/* AddTask component to add a new task */}
        {openAddTaskContent && (
          <AddTask
            tasks={tasks}
            setTasks={setTasks}
            addTask={handleAddTask}
            handleIsAddTaskContent={handleToggleAddTaskContent}
          />
        )}
        {/* TodoList component to display the list of tasks */}
        <TodoList tasks={tasks} deleteTask={handleDeleteTask} />
      </div>
      <Footer />
    </div>
  );
};

export default HomePage;
