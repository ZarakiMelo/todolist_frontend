import React from 'react';
import './Header.css'

const Header = () => {
  return (
    <header>
      <h1>Plannifiez simplement vos tâches</h1>
    </header>
  );
};

export default Header;
