import React from 'react';
import './Footer.css'

const Footer = () => {
  return (
    <footer>
      <p>&copy; 2024 My Todo List App</p>
    </footer>
  );
};

export default Footer;
