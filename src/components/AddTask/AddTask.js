import React, { useState } from 'react';
import './AddTask.css'
import Calendar from 'react-calendar';
 import 'react-calendar/dist/Calendar.css';
 import '../../styles/CustomCalendar.css';
 import { format } from 'date-fns';


const AddTask = (props) => {
    const [name, setName] = useState(null);
    const [description,setDescritpion]= useState(null);
    const [deadLine, setDeadLine] = useState(new Date());

    const reset = () => {
        setName(null);
    setDescritpion(null);
    setDeadLine(new Date())
    }
const handleReset = () => {
    reset()
}

const taskVerification = () => {
    return name && description;
  };


const handleAddTask = () =>{
    if(taskVerification()){
         const newTask = {
            name,
            description,
            deadLine: format(deadLine,"yyyy-MM-dd")
        } 
    props.addTask(newTask)
    reset()
    }
    alert("informations manquantes");
    console.log("informations manquantes");
   
}



  return (
    <div className='add_container'>
        <div className='form_container'>
            <div className='inputs_container'>
                <div className='input_container'>
                    <p className='label_input'>Nom</p>
                    <input className='name_input' onChange={(e)=>{setName(e.target.value)}} value={name}></input>
                </div>
                <div className='input_container'>
                    <p className='label_input'>Description</p>
                    <input className='description_input' onChange={(e)=>{setDescritpion(e.target.value)}} value={description}></input>
                </div>
            </div>
                <div className='calendar_container'>
                    <Calendar onChange={setDeadLine} value={deadLine} minDate={new Date()}/>
                </div>
        </div>
       
        <div className='buttons_container'>
             <button type='submit' onClick={handleAddTask}>Ajouter la tâche</button>
            <button type='reset' onClick={handleReset}>Reset</button>
            <button className='deleteCancel_button' type='button' onClick={props.handleIsAddTaskContent} >Annuler</button>

        </div>
        
    </div>
  );
};

export default AddTask;
