import React from 'react';
import './Task.css';
import { formatDate } from '../../utils/utils';

const Task = (props) => {
  const {id,name,description,deadLine,deleteTask}=props;

  const handleClick= () => {
    console.log(`Tâche "${name}" réalisée`)
  }
 console.log({"deadLine TYPE in TASKComponent :" :typeof(deadLine)})
  const handleDelete = (id) => {
    deleteTask(id)
  }
  return (
    <div className='task_container'>
      <h2 className='task_name'>{name}</h2>
      <div className='task_details'>
        <p>{description}</p>
        <p>Date limite : {formatDate(deadLine)}</p>
      </div>
      
      <div className='taskButtons_container'>
        <button type='button' onClick={handleClick}>Valider</button>
        <button className="deleteCancel_button" type='delete_button' onClick={()=>handleDelete(id)}>Supprimer</button>
      </div>
    </div>
  );
};

export default Task;
