
import axios from 'axios';

// Récupération du token d'authentification depuis les variables d'environnement
const authToken = process.env.REACT_APP_AUTH_TOKEN;

// Création d'une instance d'axios avec une configuration par défaut
const axiosInstance = axios.create({
  baseURL: 'http://localhost:1337/api', // URL de base pour les requêtes API
  headers: {
    Authorization: `Bearer ${authToken}` // Ajout du token d'authentification dans les en-têtes
  }
});

/**
 * fetchTasks
 * 
 * Fonction pour récupérer toutes les tâches depuis l'API
 * 
 * @returns {Promise} - Une promesse qui résout la réponse de l'API avec les tâches
 */
export const fetchTasks = () => {
  return axiosInstance.get('/tasks');
};

/**
 * addTask
 * 
 * Fonction pour ajouter une nouvelle tâche à l'API
 * 
 * @param {Object} newTask - L'objet représentant la nouvelle tâche à ajouter
 * @returns {Promise} - Une promesse qui résout la réponse de l'API après l'ajout de la tâche
 */
export const addTask = (newTask) => {
  return axiosInstance.post('/tasks', { data: newTask });
};

/**
 * deleteTask
 * 
 * Fonction pour supprimer une tâche de l'API
 * 
 * @param {number} id - L'identifiant de la tâche à supprimer
 * @returns {Promise} - Une promesse qui résout la réponse de l'API après la suppression de la tâche
 */
export const deleteTask = (id) => {
  return axiosInstance.delete(`/tasks/${id}`);
};
