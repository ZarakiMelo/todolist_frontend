# My Todo List Frontend

Cette application est une application de gestion de tâches (Todo List) construite avec React. Elle utilise Axios pour les requêtes HTTP et nécessite un token d'identification Strapi pour fonctionner.

## Prérequis

Assurez-vous d'avoir installé les éléments suivants :

- Node.js (version recommandée : 16.x.x ou supérieure)
- npm (version recommandée : 8.x.x ou supérieure)

## Installation

1. Clonez le dépôt sur votre machine locale :
   ```bash
   git clone <URL_DU_DEPOT>


2. Accédez au répertoire du projet :
    ```bash
    cd my-todo-list-frontend

3. Installez les dépendances du projet :
    ```bash
    npm install



## Configuration

1. Créez un fichier `.env`à la racine du projet.

2. Ajoutez votre token d'identification Strapi dans le fichier `.env` :
    ```bash
    REACT_APP_AUTH_TOKEN=<VOTRE_TOKEN_STRAPI>


## Lancer l'application

Pour démarrer l'application en mode développement, exécutez la commande suivante :
    ```bash
    npm start

L'application sera disponible sur [htt](http://localhost:3000.).


Scripts disponibles
- npm start: Démarre l'application en mode développement.
- npm run build: Compile l'application pour la production dans le dossier build.
- npm test: Lance les tests en mode interactif
- npm run eject: Ejecte la configuration de Create React App. Cette action est irréversible.

Dépendances principales
- React : 18.3.1
- Axios : 1.7.2
- date-fns : 3.6.0
- react-calendar : 5.0.0
- react-datepicker : 7.1.0
- react-dom : 18.3.1
- react-scripts : 5.0.1


Contribution
Les contributions sont les bienvenues ! Veuillez soumettre une pull request pour toute amélioration ou nouvelle fonctionnalité.


Contribution
Les contributions sont les bienvenues ! Veuillez soumettre une pull request pour toute amélioration ou nouvelle fonctionnalité.

Ce fichier `README.md` inclut des informations sur l'installation, la configuration, le démarrage de l'application, ainsi que la structure du projet et les versions des principales dépendances utilisées.


